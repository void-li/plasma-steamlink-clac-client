/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "plasma_steamlink_clac_client.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QHostInfo>
#include <QQmlEngine>

#include "lvd/shield.hpp"

#include "clac.pb.hpp"
#include "config.hpp"
#include "packets.hpp"
#include "plasma_steamlink_clac_client_state_model.hpp"

// ----------

namespace lvd::plasma::steamlink::clac::client {

PlasmaSteamlinkClacClient::PlasmaSteamlinkClacClient(QObject* parent)
    : QObject(parent) {
  LVD_LOG_T();

  idle_timer_ = new QTimer(this);
  idle_timer_->setInterval(config::Steamlink_Clac_Client_Idle_Time());
  idle_timer_->setSingleShot(true);

  connect(idle_timer_, &QTimer::timeout,
          this, &PlasmaSteamlinkClacClient::on_idle_timeout);

  void_timer_ = new QTimer(this);
  void_timer_->setInterval(config::Steamlink_Clac_Client_Void_time());
  void_timer_->setSingleShot(true);

  connect(void_timer_, &QTimer::timeout,
          this, &PlasmaSteamlinkClacClient::on_void_timeout);

  state_model_ = new StateModel(this);
  qmlRegisterInterface<PlasmaSteamlinkClacClientStateModel>("PlasmaSteamlinkClacClientStateModel", 1);
}

// ----------

void PlasmaSteamlinkClacClient::sigusr1() {
  LVD_LOG_T();

  lvd::steamlink::clac::proto::ClientMessage       client_message;

  LVD_LOG_D() << "state"
              << state_.to_string();

  if (   state_ != State::CONNECTED
      && state_ != State::CONNECTED_OTHER) {
    client_message.setType(lvd::steamlink::clac::proto::ClientMessage::   CONNECT_TO  );
    client_message.setHostname(QHostInfo::localHostName());
  } else {
    client_message.setType(lvd::steamlink::clac::proto::ClientMessage::DISCONNECT_FROM);
    client_message.setHostname(QHostInfo::localHostName());
  }

  lvd::steamlink::clac::proto::ClientMessagePacket client_message_packet(client_message);
  emit transmit(client_message_packet.serialize());
}

void PlasmaSteamlinkClacClient::sigusr2() {
  LVD_LOG_T();

  lvd::steamlink::clac::proto::ClientMessage       client_message;
  client_message.setType(lvd::steamlink::clac::proto::ClientMessage::SHUTDOWN);
  client_message.setHostname(QHostInfo::localHostName());

  lvd::steamlink::clac::proto::ClientMessagePacket client_message_packet(client_message);
  emit transmit(client_message_packet.serialize());
}

// ----------

void PlasmaSteamlinkClacClient::execute() {
  LVD_LOG_T();
  LVD_SHIELD;

  QTimer::singleShot(1024, this, [&] {
    lvd::steamlink::clac::proto::ClientMessage       client_message;
    client_message.setType(lvd::steamlink::clac::proto::ClientMessage::QUERYING);
    client_message.setHostname(QHostInfo::localHostName());

    lvd::steamlink::clac::proto::ClientMessagePacket client_message_packet(client_message);
    emit transmit(client_message_packet.serialize());
  });

  emit state_changed(state_);

  LVD_SHIELD_END;
}

// ----------

void PlasmaSteamlinkClacClient::on_message(const QByteArray& message) {
  LVD_LOG_T();
  LVD_SHIELD;

  auto daemon_packet = lvd::steamlink::clac::proto::DaemonMessagePacket(message);
  if (!daemon_packet.valid()) {
    LVD_LOG_W() << "received invalid daemon packet";
    return;
  }

  if (daemon_packet.message().hostname().isEmpty()) {
    if (   daemon_packet.message().type() != lvd::steamlink::clac::proto::DaemonMessage::DISCONNECTED
        && daemon_packet.message().type() != lvd::steamlink::clac::proto::DaemonMessage::SHUTDOWN) {
      LVD_LOG_W() << "received invalid daemon packet missing hostname";
      return;
    }
  }

  cancel_idle();
  cancel_void();

  LVD_FINALLY {
    await_void();
  };

  auto& daemon_message = daemon_packet.message();
  switch (daemon_message.type()) {
    case lvd::steamlink::clac::proto::DaemonMessage::CONNECTED:
    case lvd::steamlink::clac::proto::DaemonMessage::CONNECTED_TO:
      if (daemon_message.hostname() == QHostInfo::localHostName()) {
        state_ = State::CONNECTED;
        emit state_changed(state_);
      } else {
        state_ = State::CONNECTED_OTHER;
        emit state_changed(state_);
      }
      break;

    case lvd::steamlink::clac::proto::DaemonMessage::DISCONNECTED:
    case lvd::steamlink::clac::proto::DaemonMessage::DISCONNECTED_FROM:
      state_ = State::DISCONNECTED;
      emit state_changed(state_);
      break;

    case lvd::steamlink::clac::proto::DaemonMessage::CONNECTION_FAILED:
      state_ = State::CONNECTION_FAILED;
      emit state_changed(state_);

      await_idle();
      break;

    case lvd::steamlink::clac::proto::DaemonMessage::CONNECTION_DOOMED:
      state_ = State::CONNECTION_DOOMED;
      emit state_changed(state_);

      await_idle();
      break;

    case lvd::steamlink::clac::proto::DaemonMessage::SHUTDOWN:
      state_ = State::SHUTDOWN;
      emit state_changed(state_);
      break;

    default:
      LVD_LOG_W() << "unknown daemon message type"
                  << daemon_message.type();

      break;
  }

  LVD_SHIELD_END;
}

// ----------

void PlasmaSteamlinkClacClient::on_idle_timeout() {
  LVD_LOG_T();
  LVD_SHIELD;

  switch (state_) {
    case State::CONNECTION_FAILED:
    case State::CONNECTION_DOOMED:
      state_ = State::DISCONNECTED;
      emit state_changed(state_);

      break;

    default:
      break;
  }

  LVD_SHIELD_END;
}

void PlasmaSteamlinkClacClient::on_void_timeout() {
  LVD_LOG_T();
  LVD_SHIELD;

  state_ = State::SHUTDOWN;
  emit state_changed(state_);

  LVD_SHIELD_END;
}

}  // namespace lvd::plasma::steamlink::clac::client
