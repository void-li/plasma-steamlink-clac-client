/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QObject>
#include <QString>
#include <QVariantList>

#include <Plasma/Applet>  // IWYU pragma: keep
// IWYU pragma: no_include <plasma/applet.h>

#include "lvd/logger.hpp"

#include "plasma_steamlink_clac_client.hpp"
#include "plasma_steamlink_clac_client_network.hpp"

// ----------

namespace lvd::plasma::steamlink::clac::client {

class PlasmaSteamlinkClacClientApplet : public Plasma::Applet {
  Q_OBJECT LVD_LOGGER

 public:
  PlasmaSteamlinkClacClientApplet(QObject *parent, const QVariantList &args);

  Q_INVOKABLE PlasmaSteamlinkClacClient *nativeInterface() const {
    return plasma_steamlink_clac_client_;
  }

 private:
  PlasmaSteamlinkClacClient       * plasma_steamlink_clac_client_         = nullptr;
  PlasmaSteamlinkClacClientNetwork* plasma_steamlink_clac_client_network_ = nullptr;
};

} // namespace lvd::plasma::steamlink::clac::client
