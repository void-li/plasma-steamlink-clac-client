/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "plasma_steamlink_clac_client_applet.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QMetaObject>
#include <QQmlEngine>

#include <KPluginMetaData>  // IWYU pragma: keep
// IWYU pragma: no_include <kpluginmetadata.h>

#include "lvd/metatype.hpp"
#include "lvd/shield.hpp"

// ----------

namespace lvd::plasma::steamlink::clac::client {

PlasmaSteamlinkClacClientApplet::PlasmaSteamlinkClacClientApplet(
    QObject *parent, const QVariantList &args
)   : Plasma::Applet(parent, KPluginMetaData(), args) {
  lvd::Metatype::metatype();

  // ----------

  lvd::Logger::install();
  LVD_LOG_T();

  // ----------

  plasma_steamlink_clac_client_         = new PlasmaSteamlinkClacClient(this);
  qmlRegisterInterface<PlasmaSteamlinkClacClient>("PlasmaSteamlinkClacClient", 1);

  plasma_steamlink_clac_client_network_ = new PlasmaSteamlinkClacClientNetwork(this);
  plasma_steamlink_clac_client_network_->setup();

  connect(plasma_steamlink_clac_client_network_, &PlasmaSteamlinkClacClientNetwork::message,
          plasma_steamlink_clac_client_,         &PlasmaSteamlinkClacClient::on_message);

  connect(plasma_steamlink_clac_client_,         &PlasmaSteamlinkClacClient::transmit,
          plasma_steamlink_clac_client_network_, &PlasmaSteamlinkClacClientNetwork::on_transmit);

  QMetaObject::invokeMethod(plasma_steamlink_clac_client_, [&] {
    LVD_SHIELD;
    plasma_steamlink_clac_client_network_->setup();
    LVD_SHIELD_END;

    plasma_steamlink_clac_client_->execute();
  }, Qt::QueuedConnection);
}

} // namespace lvd::plasma::steamlink::clac::client

// ----------

K_EXPORT_PLASMA_APPLET_WITH_JSON(plasma_steamlink_clac_client_applet,
  lvd::plasma::steamlink::clac::client::PlasmaSteamlinkClacClientApplet, "metadata.json")

#include "plasma_steamlink_clac_client_applet.moc"  // IWYU pragma: keep
