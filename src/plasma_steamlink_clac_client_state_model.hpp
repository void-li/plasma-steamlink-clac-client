/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QObject>
#include <QString>

#include "lvd/logger.hpp"

#include "plasma_steamlink_clac_client.hpp"

// ----------

namespace lvd::plasma::steamlink::clac::client {

class PlasmaSteamlinkClacClientStateModel : public QObject {
  Q_OBJECT LVD_LOGGER_LIKE(PlasmaSteamlinkClacClient)

  Q_PROPERTY(QString state READ state NOTIFY state_changed)

 public:
  PlasmaSteamlinkClacClientStateModel(PlasmaSteamlinkClacClient* parent)
      : QObject(parent),
        parent_(parent) {
    connect(parent_, &PlasmaSteamlinkClacClient::state_changed,
            this, &PlasmaSteamlinkClacClientStateModel::state_changed);
  }

 public:
  QString state() const {
    return parent_->state().to_string();
  }

  Q_INVOKABLE void sigusr1() {
    parent_->sigusr1();
  }
  Q_INVOKABLE void sigusr2() {
    parent_->sigusr2();
  }

 signals:
  void state_changed();

 private:
  PlasmaSteamlinkClacClient* parent_ = nullptr;
  friend PlasmaSteamlinkClacClient;
};

}  // namespace lvd::plasma::steamlink::clac::client
