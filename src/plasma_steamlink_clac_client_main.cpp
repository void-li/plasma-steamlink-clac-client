/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <cstdlib>
#include <exception>

#include <QCommandLineParser>
#include <QMetaObject>
#include <QObject>
#include <QString>

#include "lvd/application.hpp"
#include "lvd/logger.hpp"
#include "lvd/metatype.hpp"
#include "lvd/shield.hpp"

#include "config.hpp"
#include "plasma_steamlink_clac_client.hpp"
#include "plasma_steamlink_clac_client_network.hpp"

// ----------

int main(int argc, char* argv[]) {
  lvd::Metatype::metatype();

  lvd::Application::setApplicationName (lvd::plasma::steamlink::clac::client::config::App_Name());
  lvd::Application::setApplicationVersion(lvd::plasma::steamlink::clac::client::config::App_Vers());

  lvd::Application::setOrganizationName(lvd::plasma::steamlink::clac::client::config::Org_Name());
  lvd::Application::setOrganizationDomain(lvd::plasma::steamlink::clac::client::config::Org_Addr());

  lvd::Application app(argc, argv);

  lvd::Application::connect(&app, &lvd::Application::failure,
  [] (const QString& message) {
    if (!message.isEmpty()) {
      lvd::qStdErr() << message << Qt::endl;
    }

    lvd::Application::exit(1);
  });

  // ----------

  lvd::Logger::install();
  LVD_LOGFUN

  // ----------

  QCommandLineParser qcommandlineparser;
  qcommandlineparser.   addHelpOption();
  qcommandlineparser.addVersionOption();

  lvd::Logger::setup_arguments(qcommandlineparser);

  qcommandlineparser.process(app);

  lvd::Logger::parse_arguments(qcommandlineparser);
  lvd::Application::print();

  // ----------

  try {
    using PlasmaSteamlinkClacClient        = lvd::plasma::steamlink::clac::client::PlasmaSteamlinkClacClient;
    PlasmaSteamlinkClacClient        plasma_steamlink_clac_client;

    using PlasmaSteamlinkClacClientNetwork = lvd::plasma::steamlink::clac::client::PlasmaSteamlinkClacClientNetwork;
    PlasmaSteamlinkClacClientNetwork plasma_steamlink_clac_client_network;

    QObject::connect(&plasma_steamlink_clac_client_network, &PlasmaSteamlinkClacClientNetwork::message,
                     &plasma_steamlink_clac_client,         &PlasmaSteamlinkClacClient::on_message);

    QObject::connect(&plasma_steamlink_clac_client,         &PlasmaSteamlinkClacClient::transmit,
                     &plasma_steamlink_clac_client_network, &PlasmaSteamlinkClacClientNetwork::on_transmit);

    QMetaObject::invokeMethod(&plasma_steamlink_clac_client, [&] {
      LVD_SHIELD;
      plasma_steamlink_clac_client_network.setup();
      LVD_SHIELD_END;

      plasma_steamlink_clac_client.execute();
    }, Qt::QueuedConnection);

    QObject::connect(&plasma_steamlink_clac_client, &PlasmaSteamlinkClacClient::state_changed,
                     [] (PlasmaSteamlinkClacClient::State state) {
      LVD_LOG_I() << "state" << state.to_string();
    });

    return app.exec();
  }
  catch (const std::exception& ex) {
    LVD_LOG_C() << "exception:" << ex.what();
  }
  catch (...) {
    LVD_LOG_C() << "exception!";
  }

  return EXIT_FAILURE;
}
