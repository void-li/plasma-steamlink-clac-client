/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QByteArray>
#include <QHostAddress>
#include <QMetaType>
#include <QObject>
#include <QString>
#include <QTimer>

#include "lvd/core.hpp"
#include "lvd/core_enum.hpp"
#include "lvd/logger.hpp"
#include "lvd/metatype.hpp"

// ----------

namespace lvd::plasma::steamlink::clac::client {

class PlasmaSteamlinkClacClientStateModel;

// ----------

class PlasmaSteamlinkClacClient : public QObject {
  Q_OBJECT LVD_LOGGER

  using StateModel = PlasmaSteamlinkClacClientStateModel;
  friend StateModel;
  StateModel* state_model_ = nullptr;

 public:
  LVD_ENUM(State,
    CONNECTED,
    CONNECTED_OTHER,
    DISCONNECTED,
    CONNECTION_FAILED,
    CONNECTION_DOOMED,
    SHUTDOWN
  );

  using StateEnum = State::__enum__;
  Q_ENUM(StateEnum)

 public:
  PlasmaSteamlinkClacClient(QObject* parent = nullptr);

  Q_INVOKABLE PlasmaSteamlinkClacClientStateModel* state_model() const {
    return state_model_;
  }

 public:
  State state() const {
    return state_;
  }

  void sigusr1();
  void sigusr2();

 public slots:
  void execute();

 public slots:
  void on_message(const QByteArray& message);

 signals:
  void transmit(const QByteArray&   message,
                const QHostAddress& address = QHostAddress::Broadcast);

  void state_changed(State state);

 private:
  void await_idle() {
    idle_timer_->start();
  }
  void cancel_idle() {
    idle_timer_->stop();
  }

  void await_void() {
    void_timer_->start();
  }
  void cancel_void() {
    void_timer_->stop();
  }

 private slots:
  void on_idle_timeout();
  void on_void_timeout();

 private:
  QTimer* idle_timer_ = nullptr;
  QTimer* void_timer_ = nullptr;

  State state_ = State::SHUTDOWN;
};

}  // namespace lvd::plasma::steamlink::clac::client

Q_DECLARE_METATYPE(lvd::plasma::steamlink::clac::client::PlasmaSteamlinkClacClient::State)
L_DECLARE_METATYPE(lvd::plasma::steamlink::clac::client::PlasmaSteamlinkClacClient::State, State)
