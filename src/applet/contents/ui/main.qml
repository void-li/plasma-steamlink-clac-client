/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */

import QtQuick 2.13
import QtQuick.Layouts 1.2

import org.kde.plasma.core 2.0
import org.kde.plasma.plasmoid 2.0

// ----------

Item {
  id: main

  property bool horizontal_layout: plasmoid.formFactor === Types.Horizontal

  Plasmoid.compactRepresentation: Item {
    Layout.fillHeight: main.horizontal_layout
    Layout.fillWidth: !main.horizontal_layout

    Layout.preferredHeight: childrenRect.height
    Layout.preferredWidth:  childrenRect.width

    Root {
      height: main.horizontal_layout ? parent.height -4 : width
      width: !main.horizontal_layout ? parent.width  -4 : height

      state:         plasmoid.nativeInterface.nativeInterface().state_model().state
      state_handler: plasmoid.nativeInterface.nativeInterface().state_model()

      color_connected:         theme.positiveTextColor
      color_connected_other:   theme. neutralTextColor
      color_disconnected:      theme.        linkColor
      color_connection_failed: theme.negativeTextColor
      color_connection_doomed: theme.negativeTextColor
      color_shutdown:          theme.        textColor
    }
  }
}
