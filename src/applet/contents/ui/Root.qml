/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */

import QtQuick 2.13

import QtGraphicalEffects 1.13

// ----------

Item {
  id: base

  implicitHeight: children[0].implicitHeight
  implicitWidth:  children[0].implicitWidth

  property string state: "SHUTDOWN"
  property var    state_handler

  property color color_connected
  property color color_connected_other
  property color color_disconnected
  property color color_connection_failed
  property color color_connection_doomed
  property color color_shutdown

  Image {
    anchors.fill: parent

    fillMode: Image.PreserveAspectFit

    mipmap: true
    source: "steam-brands.svg"

    ColorOverlay {
      anchors.fill: parent
      source:       parent

      color: base.state === "CONNECTED"         ? base.color_connected
           : base.state === "CONNECTED_OTHER"   ? base.color_connected_other
           : base.state === "DISCONNECTED"      ? base.color_disconnected
           : base.state === "CONNECTION_FAILED" ? base.color_connection_failed
           : base.state === "CONNECTION_DOOMED" ? base.color_connection_doomed
           : base.state === "SHUTDOWN"          ? base.color_shutdown
           :                                      base.color_shutdown

      Behavior on color {
        ColorAnimation {
          duration: 200
        }
      }
    }

    MouseArea {
      anchors.fill: parent

      onClicked: {
        state_handler.sigusr1()
      }

      onDoubleClicked: {
        state_handler.sigusr2()
      }
    }
  }
}
