/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "config.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#define CONFIG(Type, Name, Data)   \
  Type Name() {                    \
    static const Type Name = Data; \
    return Name;                   \
  }

// ----------

namespace lvd::plasma::steamlink::clac::client::config {

CONFIG(QString, App_Name, "@PROJECT_NAME@")
CONFIG(QString, App_Vers, "@LVD_VERSION@")

CONFIG(QString, Org_Name, "void.li")
CONFIG(QString, Org_Addr, "void.li")

// ----------

CONFIG(int    , Steamlink_Clac_Client_Idle_Time, 10000)
CONFIG(int    , Steamlink_Clac_Client_Void_time, 70000)

CONFIG(quint16, Steamlink_Clac_Client_Network_Client_Port, 27039)
CONFIG(quint16, Steamlink_Clac_Client_Network_Daemon_Port, 27038)

}  // namespace lvd::plasma::steamlink::clac::client::config
