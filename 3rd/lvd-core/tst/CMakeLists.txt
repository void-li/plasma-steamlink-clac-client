find_package(Qt5 REQUIRED COMPONENTS Core Test)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

include_directories(../src)

# ----------

configure_file(
  "${CMAKE_CURRENT_SOURCE_DIR}/test_config.cpp.in"
  "${CMAKE_CURRENT_BINARY_DIR}/test_config.cpp" @ONLY
)

add_library(test_config_a STATIC
  test_config.cpp.in "${CMAKE_CURRENT_BINARY_DIR}/test_config.cpp"
  test_config.hpp
)

target_link_libraries(test_config_a
  lvd-core
  Qt5::Core
)

# ----------

file(GLOB tests "*_test.cpp")
foreach(test ${tests})
  get_filename_component(test "${test}" NAME_WE)
  set(path "${test}")

  add_executable("${test}" ${${test}_SRCS}
    "${path}.cpp"
  )

  target_link_libraries("${test}" ${${test}_LIBS}
    -Wl,--whole-archive
    lvd-core
    lvd-core-test
    -Wl,--no-whole-archive
    Qt5::Core
    Qt5::Test
    test_config_a
  )

  add_test("${test}"
    "${test}"
    -o               -,txt
    -o "${test}.qu.xml,xml"
  )

  add_coverage("${test}_coverage"
    "${test}_coverage"
    "${CMAKE_CTEST_COMMAND}" -R "${test}"
  )
endforeach()

# ----------

add_coverage(test_coverage
  test_coverage
  "${CMAKE_CTEST_COMMAND}"
)
