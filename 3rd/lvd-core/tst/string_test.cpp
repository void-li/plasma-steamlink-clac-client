/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "core.hpp"  // IWYU pragma: keep

#include <QByteArray>
#include <QObject>
#include <QString>
#include <QStringRef>

#include "string.hpp"

#include "test.hpp"
using namespace lvd;
using namespace lvd::test;

// ----------

class StringTest : public Test {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void lstrip() {
    QFETCH(QString, string);
    QFETCH(QString, result);
    QFETCH(QString,   skip);
    QFETCH(int    ,  limit);

    QCOMPARE(::lstrip(string, skip, limit).toString(), result);
  }

  void lstrip_data(bool add_column = true) {
    if (add_column) {
      QTest::addColumn<QString>("string");
      QTest::addColumn<QString>("result");
      QTest::addColumn<QString>(  "skip");
      QTest::addColumn<int    >( "limit");
    }

    QTest::newRow(  "Hello World!") <<   "Hello World!" <<   "Hello World!" << " " << -1;
    QTest::newRow( " Hello World!") <<  " Hello World!" <<   "Hello World!" << " " << -1;
    QTest::newRow("  Hello World!") << "  Hello World!" <<   "Hello World!" << " " << -1;
    QTest::newRow("##Hello World!") << "##Hello World!" <<   "Hello World!" << "#" << -1;
    QTest::newRow("##Hello World!") << "##Hello World!" << "##Hello World!" << "#" <<  0;
    QTest::newRow("##Hello World!") << "##Hello World!" <<  "#Hello World!" << "#" <<  1;
    QTest::newRow(" #Hello World!") << " #Hello World!" << " #Hello World!" << "#" << -1;
    QTest::newRow("")               << "              " << ""               << " " << -1;
  }

  void lstrip_default() {
    QString string = " \nHello World!";
    QString result =    "Hello World!";

    QCOMPARE(::lstrip(string).toString(), result);
  }

  void lstrip_qstringref() {
    QString    string = " \nHello World!";
    QString    result =    "Hello World!";

    QStringRef thingy(&string);

    QCOMPARE(::lstrip(thingy).toString(), result);
  }

  void lstrip_qbytearray() {
    QByteArray string = " \nHello World!";
    QByteArray result =    "Hello World!";

    QCOMPARE(::lstrip(string)           , result);
  }

  // ----------

  void rstrip() {
    QFETCH(QString, string);
    QFETCH(QString, result);
    QFETCH(QString,   skip);
    QFETCH(int    ,  limit);

    QCOMPARE(::rstrip(string, skip, limit).toString(), result);
  }

  void rstrip_data(bool add_column = true) {
    if (add_column) {
      QTest::addColumn<QString>("string");
      QTest::addColumn<QString>("result");
      QTest::addColumn<QString>(  "skip");
      QTest::addColumn<int    >( "limit");
    }

    QTest::newRow("Hello World!"  ) << "Hello World!"   << "Hello World!"   << " " << -1;
    QTest::newRow("Hello World! " ) << "Hello World! "  << "Hello World!"   << " " << -1;
    QTest::newRow("Hello World!  ") << "Hello World!  " << "Hello World!"   << " " << -1;
    QTest::newRow("Hello World!##") << "Hello World!##" << "Hello World!"   << "#" << -1;
    QTest::newRow("Hello World!##") << "Hello World!##" << "Hello World!##" << "#" <<  0;
    QTest::newRow("Hello World!##") << "Hello World!##" << "Hello World!#"  << "#" <<  1;
    QTest::newRow("Hello World!# ") << "Hello World!# " << "Hello World!# " << "#" << -1;
    QTest::newRow("")               << "              " << ""               << " " << -1;
  }

  void rstrip_default() {
    QString string = "Hello World! \n";
    QString result = "Hello World!"   ;

    QCOMPARE(::rstrip(string).toString(), result);
  }

  void rstrip_qstringref() {
    QString    string = "Hello World! \n";
    QString    result = "Hello World!"   ;

    QStringRef thingy(&string);

    QCOMPARE(::rstrip(thingy).toString(), result);
  }

  void rstrip_qbytearray() {
    QByteArray string = "Hello World! \n";
    QByteArray result = "Hello World!"   ;

    QCOMPARE(::rstrip(string)           , result);
  }

  // ----------

  void  strip() {
    QFETCH(QString, string);
    QFETCH(QString, result);
    QFETCH(QString,   skip);
    QFETCH(int    ,  limit);

    QCOMPARE(:: strip(string, skip, limit).toString(), result);
  }

  void  strip_data() {
    QTest::addColumn<QString>("string");
    QTest::addColumn<QString>("result");
    QTest::addColumn<QString>(  "skip");
    QTest::addColumn<int    >( "limit");

    lstrip_data(false);
    rstrip_data(false);

    QTest::newRow(  "Hello World##") <<   "Hello World##" << "Hello World" << "#" << -1;
    QTest::newRow( "#Hello World##") <<  "#Hello World##" << "Hello World" << "#" << -1;
    QTest::newRow("##Hello World##") << "##Hello World##" << "Hello World" << "#" << -1;
    QTest::newRow("##Hello World#" ) << "##Hello World#"  << "Hello World" << "#" << -1;
    QTest::newRow("##Hello World"  ) << "##Hello World"   << "Hello World" << "#" << -1;
  }

  // ----------

 private slots:
  void init() {
    Test::init();
  }

  void initTestCase() {
    Test::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      Test::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(StringTest)
#include "string_test.moc"  // IWYU pragma: keep
