project(Lvd-Core CXX)
cmake_minimum_required(VERSION 3.13)

set(CMAKE_CXX_STANDARD 20)
include(lvd.cmake)
enable_testing()

add_subdirectory(3rd)
add_subdirectory(pkg)
add_subdirectory(src)

if(NOT "${CMAKE_BUILD_TYPE}" STREQUAL "Release")
  if(CMAKE_SOURCE_DIR STREQUAL PROJECT_SOURCE_DIR)
    add_subdirectory(tst)
  endif()
endif()
